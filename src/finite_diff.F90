!! Copyright (C) 2011 M. Oliveira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

module finite_diff_m
  use global_m
  use oct_parser_m
  use messages_m
  use linalg_m
  implicit none


                    !---Interfaces---!

  interface assignment (=)
     module procedure fd_operator_copy
  end interface


                    !---Derived Data Types---!

  type fd_operator_t
    private
    integer :: order
    integer :: np    
    integer :: size
    integer,  pointer :: f_index(:)
    integer,  pointer :: c_index(:)
    real(R8), pointer :: c(:)
  end type fd_operator_t


                    !---Public/Private Statements---!

  private
  public :: fd_operator_t, &
            fd_operator_null, &
            fd_operator_init, &
            assignment(=), &
            fd_operator_apply, &
            fd_operator_end


contains

  !-----------------------------------------------------------------------
  !> Nullifies and sets to zero all the components of operator op.         
  !-----------------------------------------------------------------------
  subroutine fd_operator_null(op)
    type(fd_operator_t), intent(out) :: op

    call push_sub("fd_operator_null")

    op%np = 0
    op%order = 0
    op%size = 0
    nullify(op%f_index)
    nullify(op%c_index)
    nullify(op%c)

    call pop_sub()
  end subroutine fd_operator_null

  !-----------------------------------------------------------------------
  !> Initializes operator op, by calculating the finite differences       
  !> coefficients and the lookup tables to speed-up the application of the
  !-----------------------------------------------------------------------
  subroutine fd_operator_init(op, order, n, np, x)
    type(fd_operator_t), intent(inout) :: op    !< operator
    integer,             intent(in)    :: order !< derivative order
    integer,             intent(in)    :: n     !< number of points in each spatial direction to be used for
                                                !! the calculation of the derivative at a given point
    integer,             intent(in)    :: np    !< number of points in the mesh
    real(R8),            intent(in)    :: x(np) !< mesh

    integer :: i, j, nl, nr, il, ir, n_stencil

    call push_sub("fd_operator_init")

    op%order = op%order
    op%np = np
    op%size = np*(n*2+1)
    allocate(op%c(op%size))
    allocate(op%c_index(op%size))
    allocate(op%f_index(op%size))

    do i = 1, np
      !Number of points to the left and to the right
      if (i > n .and. i < np-n) then
        nl = n
        nr = n
      else if (i >= np-n) then
        nr = np-i
        nl = min(2*n-nr, i-1)
      else if (i <= n) then
        nl = i-1
        nr = min(2*n-nl, np-i)
      end if

      !Size and boundaries of the stencil
      n_stencil = nl + nr + 1
      il = n_stencil*(i - 1) + 1
      ir = il + n_stencil - 1

      !Lookup tables
      op%f_index(il:ir) = i
      do j = 0, nl + nr
        op%c_index(il + j) = i - nl + j
      end do

      !Coefficients
      call fd_coeff(order, n_stencil, nl+1, x(i-nl:i+nr)-x(i), op%c(il:ir))
    end do

    call pop_sub()
  end subroutine fd_operator_init

  !-----------------------------------------------------------------------
  !> Calculates the finite differences coefficients for a given set of    
  !> points.                                                              
  !-----------------------------------------------------------------------
  subroutine fd_coeff(order, n, i0, h, coeff)
    integer,         intent(in)    :: order    !< derivative order
    integer,         intent(in)    :: n        !< number of points
    integer,         intent(in)    :: i0       !< position of the point where the derivative is calculated
    real(R8),        intent(in)    :: h(n)     !< distances of the points (note that should be h(i0) = 0)
    real(R8),        intent(out)   :: coeff(n) !< the coefficients

    integer :: i, j, fac
    real(R8), allocatable :: e(:), c(:), hi(:), g(:,:)

    call push_sub("fd_coeff")
    
    allocate(e(n-1), g(n-1, n-1), c(n-1), hi(n-1))

    fac = 1
    e = M_ZERO
    do i = 1, n-1
      fac = fac*i
      if (i == order) then
        e(i) = fac
        exit
      end if
    end do

    hi(1:i0-1) = h(1:i0-1)
    hi(i0:n-1) = h(i0+1:n)
    g(1, 1:n-1) = hi(1:n-1)
    do i = 2, n-1
      do j = 1, n-1
        g(i, j) = g(i-1, j)*hi(j)
      end do
    end do

    call solve_linear_system(n-1, g, e, c)
    coeff(1:i0-1) = c(1:i0-1)
    coeff(i0) = -sum(c)
    coeff(i0+1:n) = c(i0:n-1)

    deallocate(e, g, c, hi)

    call pop_sub()
  end subroutine fd_coeff

  !-----------------------------------------------------------------------
  !> Copies operator op_b to op_a.                                        
  !-----------------------------------------------------------------------
  subroutine fd_operator_copy(op_a, op_b)
    type(fd_operator_t), intent(inout) :: op_a
    type(fd_operator_t), intent(in)    :: op_b

    call push_sub("fd_operator_copy")

    call fd_operator_end(op_a)
    op_a%order = op_b%order
    op_a%np = op_b%np
    op_a%size = op_b%size
    if (op_a%size > 0) then
      allocate(op_a%c(op_a%size))
      allocate(op_a%c_index(op_a%size))
      allocate(op_a%f_index(op_a%size))
      op_a%c = op_b%c
      op_a%c_index = op_b%c_index
      op_a%f_index = op_b%f_index
    end if

    call pop_sub()
  end subroutine fd_operator_copy

    !-----------------------------------------------------------------------
    !> Calculate the derivative of f by applying the operator op to function 
    !> f.                                                                    
    !-----------------------------------------------------------------------
  subroutine fd_operator_apply(op, f, opf)
    type(fd_operator_t), intent(in)  :: op         !< operator
    real(R8),            intent(in)  :: f(op%np)   !< function
    real(R8),            intent(out) :: opf(op%np) !< derivative of f
    
    integer :: i

    call push_sub("fd_operator_apply")

    opf = M_ZERO
    do i = 1, op%size
      opf(op%f_index(i)) = opf(op%f_index(i)) + f(op%c_index(i))*op%c(i)
    end do

    call pop_sub()
  end subroutine fd_operator_apply

  !-----------------------------------------------------------------------
  !> Frees all memory associated to operator op.                          
  !-----------------------------------------------------------------------
  subroutine fd_operator_end(op)
    type(fd_operator_t), intent(inout) :: op

    call push_sub("fd_operator_end")
    
    if (associated(op%c))       deallocate(op%c)
    if (associated(op%c_index)) deallocate(op%c_index)
    if (associated(op%f_index)) deallocate(op%f_index)
    op%np = 0
    op%order = 0
    op%size = 0

    call pop_sub()
  end subroutine fd_operator_end

end module finite_diff_m
