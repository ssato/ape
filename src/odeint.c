/*
 Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
 Copyright (C) 2011 T. Cerqueira

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

*/

#include <config.h>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_odeiv.h>
#include <math.h>


struct ode_type_struct
{
  void (* fdf) (void *func_params, double *pt, double *py, double *pf);
  double *pt, *py, *pf;
  int *s_dim;
  void *func_params;
};

typedef struct ode_type_struct ode_type;


int f (double t, const double y[], double z[], void *params)
{
  int i;
  ode_type *p = (ode_type *)params;
  p->pt = &t;
  
  for (i = 0; i < *(p->s_dim); i++) {	
    p->py[i] = y[i];
  }

  (*(p->fdf))(p->func_params, p->pt, p->py, p->pf);
		
  for (i = 0; i < *(p->s_dim); i++) {	
    z[i] = p->pf[i];
  }
  return GSL_SUCCESS;
}


int FC_FUNC_ (ode_solve, ODE_SOLVE)
     (double *t1, void **s, void **e, void **c, double *h1, int *nstepmax, 
      int *nstep, double x[], int *s_dim, double *y1, void *func_params, 
      void(* forfunc)(void *, double *, double *, double *) )
{
  double t, y[*s_dim], h, sign;
  int i, j, status;
  ode_type p;	
  p.fdf = forfunc;
  p.s_dim = s_dim;
  p.func_params = func_params;
  p.py =  malloc(*s_dim * sizeof(double));	
  p.pf =  malloc(*s_dim * sizeof(double));	

  sign = (*t1-x[0])/fabs(*t1-x[0]);
  h = sign*(*h1);
  gsl_odeiv_system sys = { f, NULL, *s_dim, &p };

  status = gsl_odeiv_evolve_reset ((gsl_odeiv_evolve *)(*e));
  if ( status != GSL_SUCCESS ) return(status);

  i = 0;
  t = x[0];
		
  for (j = 0; j < *s_dim; j++) {
    y[j]   = *(y1+*nstepmax*j);
  }

  while ( (*t1-t)*sign > 0.0 ) {

    status = gsl_odeiv_evolve_apply( (gsl_odeiv_evolve *)(*e), 
	     (gsl_odeiv_control *)(*c), (gsl_odeiv_step *)(*s), &sys, &t, *t1, &h, y );
    if ( status != GSL_SUCCESS ) break;
    i += 1;
    x[i] = t;
	
    for (j = 0; j < *s_dim; j++) {
      *(y1+*nstepmax*j+i) = y[j];
    }

    //Check if the maximum number of steps has been reached
    if ( i+1 > *nstepmax ) {
      status = GSL_EMAXITER;
      break;
    }

  }

  *nstep = i+1;

  free(p.pf);
  free(p.py);

  return (status);
}
