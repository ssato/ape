!! Copyright (C) 2004-2015 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

#include "global.h"

! This module contains the intefaces for the oct_parser library.

module oct_parser_m
  use iso_c_binding
  implicit none


                    !---Public/Private Statements---!

  private
  public :: oct_parse_f90_init, &
            oct_parse_f90_end, &
            oct_parse_f90_putsym, &
            oct_parse_f90_input, &
            oct_parse_f90_isdef, &
            oct_parse_f90_int, &
            oct_parse_f90_double, &
            oct_parse_f90_complex, &
            oct_parse_f90_string, &
            oct_parse_f90_logical, &
            oct_parse_f90_block, &
            oct_parse_f90_block_end, &
            oct_parse_f90_block_n, &
            oct_parse_f90_block_cols, &
            oct_parse_f90_block_logical, &
            oct_parse_f90_block_int, &
            oct_parse_f90_block_double, &
            oct_parse_f90_block_complex, &
            oct_parse_f90_block_string, &
            oct_parse_f90_sym_output_table



                    !---Interfaces---!

  interface
    subroutine oct_parse_f90_end() bind(c, name="oct_parse_end")
      use iso_c_binding
    end subroutine oct_parse_f90_end
  end interface

  interface oct_parse_f90_putsym
    module procedure oct_parse_f90_putsym_int, oct_parse_f90_putsym_double, oct_parse_f90_putsym_complex
  end interface


contains

  integer function oct_parse_f90_init(file_out, mpiv_node) result(ierr)
    interface
      integer(c_int) function oct_parse_init(file_out, mpiv_node) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)        :: file_out(*)
        integer(c_int),         intent(in), value :: mpiv_node
      end function oct_parse_init
    end interface
    character(len=*), intent(in) :: file_out
    integer,          intent(in) :: mpiv_node

    ierr = oct_parse_init(f_to_c_string(file_out), mpiv_node)

  end function oct_parse_f90_init


  subroutine oct_parse_f90_putsym_int(sym, i)
    interface
      subroutine oct_parse_putsym_int(sym, i) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)        :: sym(*)
        integer(c_int),         intent(in), value :: i
      end subroutine oct_parse_putsym_int
    end interface
    character(len=*), intent(in) :: sym
    integer,          intent(in) :: i

    call oct_parse_putsym_int(f_to_c_string(sym), i)

  end subroutine oct_parse_f90_putsym_int


  subroutine oct_parse_f90_putsym_double(sym, d)
    interface
      subroutine oct_parse_putsym_double(sym, d) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)        :: sym(*)
        real(c_double),         intent(in), value :: d
      end subroutine oct_parse_putsym_double
    end interface
    character(len=*), intent(in) :: sym
    real(8),          intent(in) :: d

    call oct_parse_putsym_double(f_to_c_string(sym), d)

  end subroutine oct_parse_f90_putsym_double


  subroutine oct_parse_f90_putsym_complex(sym, z)
    interface
      subroutine oct_parse_putsym_complex(sym, z) bind(c)
        use iso_c_binding
        character(kind=c_char),    intent(in)        :: sym(*)
        complex(c_double_complex), intent(in), value :: z
      end subroutine oct_parse_putsym_complex
    end interface
    character(len=*), intent(in) :: sym
    complex(8),       intent(in) :: z

    call oct_parse_putsym_complex(f_to_c_string(sym), z)

  end subroutine oct_parse_f90_putsym_complex


  integer function oct_parse_f90_input(file_in) result(ierr)
    interface 
      integer(c_int) function oct_parse_input(c_file_in) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)  :: c_file_in(*)
      end function oct_parse_input
    end interface
    character(len=*), intent(in) :: file_in

    ierr = oct_parse_input(f_to_c_string(file_in))
    
  end function oct_parse_f90_input


  logical function oct_parse_f90_isdef(name)
    interface 
      integer(c_int) function oct_parse_isdef(name) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in) :: name(*)
      end function oct_parse_isdef
    end interface
    character(len=*), intent(in) :: name

    oct_parse_f90_isdef = (oct_parse_isdef(f_to_c_string(name)) /= 0)

  end function oct_parse_f90_isdef


  logical function oct_parse_f90_logical(name, default)
    character(len=*), intent(in)  :: name
    logical,          intent(in)  :: default
    
    integer :: idefault
    
    idefault = 0
    if (default) idefault = 1

    oct_parse_f90_logical = oct_parse_f90_int(name, idefault) /= 0
    
  end function oct_parse_f90_logical


  integer function oct_parse_f90_int(name, default)
    interface
      integer(c_int) function oct_parse_int(name, default) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)         :: name(*)
        integer(c_int),         intent(in),  value :: default
      end function oct_parse_int
    end interface
    character(len=*), intent(in)  :: name
    integer,          intent(in)  :: default

    oct_parse_f90_int = oct_parse_int(f_to_c_string(name), default)

  end function oct_parse_f90_int


  real(8) function oct_parse_f90_double(name, default)
    interface
      real(c_double) function oct_parse_double(name, default) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)         :: name(*)
        real(c_double),         intent(in),  value :: default
      end function oct_parse_double
    end interface
    character(len=*), intent(in)  :: name
    real(8),          intent(in)  :: default

    oct_parse_f90_double = oct_parse_double(f_to_c_string(name), default)

  end function oct_parse_f90_double


  complex(8) function oct_parse_f90_complex(name, default)
    interface
      complex(c_double_complex) function oct_parse_complex(name, default) bind(c)
        use iso_c_binding
        character(kind=c_char),    intent(in)         :: name(*)
        complex(c_double_complex), intent(in),  value :: default
      end function oct_parse_complex
    end interface
    character(len=*), intent(in)  :: name
    complex(8),       intent(in)  :: default

    oct_parse_f90_complex = oct_parse_complex(f_to_c_string(name), default)

  end function oct_parse_f90_complex


  character(len=120) function oct_parse_f90_string(name, default)
    interface
      type(c_ptr) function oct_parse_string(name, default) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)  :: name(*)
        character(kind=c_char), intent(in)  :: default(*)
      end function oct_parse_string
    end interface
    character(len=*), intent(in)  :: name
    character(len=*), intent(in)  :: default

    character(kind=c_char,len=1) :: c_default(len_trim(default)+1)

    c_default = f_to_c_string(default)    
    call c_to_f_string_ptr(oct_parse_string(f_to_c_string(name), c_default), oct_parse_f90_string)

  end function oct_parse_f90_string


  integer function oct_parse_f90_block(name, blk) result(ierr)
    interface
      integer(c_int) function oct_parse_block(name, blk) bind(c)
        use iso_c_binding
        character(kind=c_char), intent(in)  :: name(*)
        type(c_ptr),            intent(out) :: blk
      end function oct_parse_block
    end interface
    character(len=*), intent(in)  :: name
    type(c_ptr),      intent(out) :: blk

    ierr = oct_parse_block(f_to_c_string(name), blk)

  end function oct_parse_f90_block


  subroutine oct_parse_f90_block_end(blk)
    interface
      subroutine oct_parse_block_end(blk) bind(c)
        use iso_c_binding
        type(c_ptr), intent(in) :: blk
      end subroutine oct_parse_block_end
    end interface
    type(c_ptr), intent(in) :: blk

    call oct_parse_block_end(blk)

  end subroutine oct_parse_f90_block_end


  integer function oct_parse_f90_block_n(blk)
    interface 
      integer(c_int) function oct_parse_block_n(blk) bind(c)
        use iso_c_binding
        type(c_ptr), intent(in), value :: blk
      end function oct_parse_block_n
    end interface
    type(c_ptr), intent(in) :: blk

    oct_parse_f90_block_n = oct_parse_block_n(blk)

  end function oct_parse_f90_block_n


  integer function oct_parse_f90_block_cols(blk, line)
    interface 
      integer(c_int) function oct_parse_block_cols(blk, line) bind(c)
        use iso_c_binding
        type(c_ptr),    intent(in), value :: blk
        integer(c_int), intent(in), value :: line
      end function oct_parse_block_cols
    end interface
    type(c_ptr), intent(in) :: blk
    integer,     intent(in) :: line

    oct_parse_f90_block_cols = oct_parse_block_cols(blk, line)

  end function oct_parse_f90_block_cols


  integer function oct_parse_f90_block_logical(blk, line, column, res) result(ierr)
    type(c_ptr), intent(in)  :: blk
    integer,     intent(in)  :: line
    integer,     intent(in)  :: column
    logical,     intent(out) :: res

    integer :: ires
    
    ierr = oct_parse_f90_block_int(blk, line, column, ires)
    res = (ires /= 0)
    
  end function oct_parse_f90_block_logical


  integer function oct_parse_f90_block_int(blk, line, column, res) result(ierr)
    interface 
      integer(c_int) function oct_parse_block_int(blk, line, column, res) bind(c)
        use iso_c_binding
        type(c_ptr),    intent(in),  value :: blk
        integer(c_int), intent(in),  value :: line
        integer(c_int), intent(in),  value :: column
        integer(c_int), intent(out)        :: res
      end function oct_parse_block_int
    end interface
    type(c_ptr), intent(in)  :: blk
    integer,     intent(in)  :: line
    integer,     intent(in)  :: column
    integer,     intent(out) :: res

    ierr = oct_parse_block_int(blk, line, column, res)

  end function oct_parse_f90_block_int


  integer function oct_parse_f90_block_double(blk, line, column, res) result(ierr)
    interface 
      integer(c_int) function oct_parse_block_double(blk, line, column, res) bind(c)
        use iso_c_binding
        type(c_ptr),    intent(in),  value :: blk
        integer(c_int), intent(in),  value :: line
        integer(c_int), intent(in),  value :: column
        real(c_double), intent(out)        :: res
      end function oct_parse_block_double
    end interface
    type(c_ptr), intent(inout) :: blk
    integer,     intent(in)    :: line
    integer,     intent(in)    :: column
    real(8),     intent(out)   :: res

    ierr = oct_parse_block_double(blk, line, column, res)

  end function oct_parse_f90_block_double


  integer function oct_parse_f90_block_complex(blk, line, column, res) result(ierr)
    interface 
      integer(c_int) function oct_parse_block_complex(blk, line, column, res) bind(c)
        use iso_c_binding
        type(c_ptr),               intent(in),  value :: blk
        integer(c_int),            intent(in),  value :: line
        integer(c_int),            intent(in),  value :: column
        complex(c_double_complex), intent(out)        :: res
      end function oct_parse_block_complex
    end interface
    type(c_ptr), intent(inout) :: blk
    integer,     intent(in)    :: line
    integer,     intent(in)    :: column
    complex(8),  intent(out)   :: res

    ierr = oct_parse_block_complex(blk, line, column, res)

  end function oct_parse_f90_block_complex


  integer function oct_parse_f90_block_string(blk, line, column, res) result(ierr)
    interface 
      integer(c_int) function oct_parse_block_string(blk, line, column, res) bind(c)
        use iso_c_binding
        type(c_ptr),            intent(in),  value :: blk
        integer(c_int),         intent(in),  value :: line
        integer(c_int),         intent(in),  value :: column
        type(c_ptr),            intent(out)        :: res
      end function oct_parse_block_string
    end interface
    type(c_ptr),      intent(in)  :: blk
    integer,          intent(in)  :: line
    integer,          intent(in)  :: column
    character(len=*), intent(out) :: res

    type(c_ptr) :: c_res

    ierr = oct_parse_block_string(blk, line, column, c_res)
    call c_to_f_string_ptr(c_res, res)

  end function oct_parse_f90_block_string


  subroutine oct_parse_f90_sym_output_table(only_unused)
    interface
      subroutine oct_parse_sym_output_table(only_unused) bind(c)
        use iso_c_binding
        integer(c_int), intent(in), value :: only_unused
      end subroutine oct_parse_sym_output_table
    end interface
    logical, intent(in) :: only_unused

    integer :: ionly_unused

    ionly_unused = 0
    if (only_unused) ionly_unused = 1

    call oct_parse_sym_output_table(ionly_unused)

  end subroutine oct_parse_f90_sym_output_table


  ! Helper functions to convert between C and Fortran strings
  ! Based on the routines by Joseph M. Krahn
  function f_to_c_string(f_string) result(c_string)
    use iso_c_binding
    character(len=*), intent(in) :: f_string
    character(kind=c_char,len=1) :: c_string(len_trim(f_string)+1)
    
    integer :: i, strlen

    strlen = len_trim(f_string)

    forall (i=1:strlen)
      c_string(i) = f_string(i:i)
    end forall
    c_string(strlen+1) = C_NULL_CHAR

  end function f_to_c_string

  subroutine c_to_f_string(c_string, f_string)
    use iso_c_binding
    character(kind=c_char,len=1), intent(in)  :: c_string(*)
    character(len=*),             intent(out) :: f_string

    integer :: i

    i = 1
    do while(c_string(i) /= C_NULL_CHAR .and. i <= len(f_string))
      f_string(i:i) = c_string(i)
      i = i + 1
    end do
    if (i < len(f_string)) f_string(i:) = ' '

  end subroutine c_to_f_string

  subroutine c_to_f_string_ptr(c_string, f_string)
    type(c_ptr),      intent(in)  :: c_string
    character(len=*), intent(out) :: f_string

    character(len=1, kind=c_char), pointer :: p_chars(:)
    integer :: i

    if (.not. c_associated(c_string)) then
      f_string = ' '
    else
      call c_f_pointer(c_string, p_chars, [huge(0)])
      i = 1
      do while(p_chars(i) /= C_NULL_CHAR .and. i <= len(f_string))
        f_string(i:i) = p_chars(i)
        i = i + 1
      end do
      if (i < len(f_string)) f_string(i:) = ' '
    end if

  end subroutine c_to_f_string_ptr

end module oct_parser_m
