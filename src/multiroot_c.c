/*
 Copyright (C) 2004-2009 M. Oliveira, F. Nogueira

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
 02110-1301, USA.

*/

#include <config.h>

#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_multiroots.h>

struct mr_type_struct
{
  void (* func) (int *n, double *px, double *pf);
  int *n;
  double px[10], pf[10];
};

typedef struct mr_type_struct mr_type;

static void my_work_f(mr_type *p)
{
  (*(p->func))(p->n, p->px, p->pf);
}

int my_f (const gsl_vector *x, void *params, gsl_vector *f)
{
  mr_type *p = (mr_type *)params;
  int i;
  int n = x->size;

  p->n = &n;
  for (i = 0; i < n; i++)  p->px[i] = gsl_vector_get(x, i);
  my_work_f( p );
  for (i = 0; i < n; i++)  gsl_vector_set (f, i, p->pf[i]);
  return GSL_SUCCESS;
}

void get_current(double x[], double f[], const gsl_multiroot_fsolver * s)
{
  int i;
  const int n = s->x->size;

  for (i = 0; i < n; i++) x[i] = gsl_vector_get(s->x, i);
  for (i = 0; i < n; i++) f[i] = gsl_vector_get(s->f, i);
}

int test_convergence (const double tol_x_abs, const double tol_x_rel, const double tol_f_abs, const gsl_multiroot_fsolver * s)
{
  int i, status;
  int ok = 1;
  const int n = s->x->size;

  status = gsl_multiroot_test_delta (s->dx, s->x, tol_x_abs, tol_x_rel);

  for (i = 0 ; i < n ; i++)
    {
      double fi = gsl_vector_get(s->f, i);
	
      if (fabs(fi) < tol_f_abs)
        {
          ok = 1;
        }
      else
	{
	  ok = 0;
	  break;
	}
    }
    
  if (ok || status == GSL_SUCCESS) return(GSL_SUCCESS);

  return (GSL_CONTINUE);
}

int FC_FUNC_ (non_linear_system_solve, NON_LINEAR_SYSTEM_SOLVE)
     (void **s, int *n, double *tol_x_abs, double *tol_x_rel, double *tol_f_abs, int *max_iter, void **x_guess, double x[], void(* forfunc)(int *, double *, double *), void(* write_info)(int *, int *, double *, double *))
{
  int status, iter = 0;
  double f[*n];
  mr_type p;

  //Set parameters to be passed to the function
  p.func = forfunc;

  //Initialize objects
  gsl_multiroot_function mr_f = {&my_f, *n, &p};
  gsl_multiroot_fsolver_set ((gsl_multiroot_fsolver *)(*s), &mr_f, (gsl_vector *)(*x_guess));

  do
    {
      //Iterate
      iter++;
      status = gsl_multiroot_fsolver_iterate ((gsl_multiroot_fsolver *)(*s));
      get_current(x, f, (gsl_multiroot_fsolver *)(*s));

      //Check if a problem occurred
      if (status) break;

      //Print information
      write_info (&iter, n, x, f);

      //Check convergence criteria
      status = test_convergence(*tol_x_abs, *tol_x_rel, *tol_f_abs, (gsl_multiroot_fsolver *)(*s));

      //Check if solver is stuck   
      if ((status == GSL_CONTINUE) && (iter >= *max_iter)) status = GSL_EMAXITER;
    }
  while (status == GSL_CONTINUE);

  return (status);
}
