!! Copyright (C) 2004-2010 M. Oliveira, F. Nogueira
!!
!! This program is free software; you can redistribute it and/or modify
!! it under the terms of the GNU General Public License as published by
!! the Free Software Foundation; either version 2, or (at your option)
!! any later version.
!!
!! This program is distributed in the hope that it will be useful,
!! but WITHOUT ANY WARRANTY; without even the implied warranty of
!! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!! GNU General Public License for more details.
!!
!! You should have received a copy of the GNU General Public License
!! along with this program; if not, write to the Free Software
!! Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
!! 02110-1301, USA.
!!

module units_m
  use global_m
  use oct_parser_m
  use messages_m
  implicit none


                    !---Derived Data Types---!

  !> Defines a unit and it`s conversion factor to atomic units, such that:
  !!
  !!  [a.u.] = <input> * unit%factor
  !!  <output> = [a.u.] / unit%factor
  type unit_type
    real(R8)      :: factor !< Conversion factor to atomic units
    character(15) :: name   !< Name of unit (ex. "rydberg")
    character(2)  :: abbrev !< Unit abreviation (ex. "Ry")
  end type unit_type

  !> Defines a unit system. It includes lenght and energy units.
  type unit_system_type
    character(30)   :: name   !< The name of the unit system (ex. "Atomic Units")
    type(unit_type) :: length !< The unit of length
    type(unit_type) :: energy !< The unit of energy
  end type unit_system_type


                    !---Global Variables---!

  type(unit_system_type) :: units_out, units_in


                    !---Public/Private Statements---!

  private
  public :: unit_type, unit_system_type, units_init, units_out, units_in

contains

  !-----------------------------------------------------------------------
  !>  This subroutine reads the input options concerning the input and     
  !>  output unit systems to be used and call the subroutine to initialize 
  !>  them.                                                                
  !-----------------------------------------------------------------------
  subroutine units_init()

    integer :: iunits_in, iunits_out

    message(1) = ""
    message(2) = "Setting units"
    call write_info(2)

    !Read input options
    if (oct_parse_f90_isdef("Units")) then
      iunits_in = oct_parse_f90_int("Units", 1)
      iunits_out = iunits_in
    else
      iunits_in = oct_parse_f90_int('UnitsInput', 1)
      iunits_out = oct_parse_f90_int('UnitsOutput', 1)
    end if

    !Initialize the input units
    call set_units(iunits_in, units_in)
    write(message(1),'(2X,"Input units system:  ",A)') units_in%name
    call write_info(1,20)

    !Initialize the output units
    call set_units(iunits_out, units_out)
    write(message(1),'(2X,"Output units system: ",A)') units_out%name
    call write_info(1,20)

  end subroutine units_init

  !-----------------------------------------------------------------------
  !> Initialize a unit system by setting the conversion factors and the    
  !> units name.                                                           
  !-----------------------------------------------------------------------
  subroutine set_units(iunit, unit_system)
    integer,                intent(in)  :: iunit        !< integer specifying the unit system to use
    type(unit_system_type), intent(out) :: unit_system  !< unit system to be initialized

    select case (iunit)
    case (1) !Atomic units
      unit_system%name="Atomic Units"

      unit_system%length%factor = M_ONE
      unit_system%length%name = "bohr"
      unit_system%length%abbrev = "b "

      unit_system%energy%factor = M_ONE
      unit_system%energy%name = "hartree"
      unit_system%energy%abbrev = "H "

    case (2) !Rydberg atomic units
      unit_system%name="Atomic Rydberg Units"

      unit_system%length%factor = M_ONE
      unit_system%length%name = "bohr"
      unit_system%length%abbrev = "b "

      unit_system%energy%factor = M_HALF
      unit_system%energy%name = "rydberg"
      unit_system%energy%abbrev = "Ry"

    case (3) !eV and Angstrom
      unit_system%name="Angstroms and electron volts"

      unit_system%length%factor = M_ONE/0.529177248_r8
      unit_system%length%name = "Angstrom"
      unit_system%length%abbrev = "A "

      unit_system%energy%factor = M_ONE/27.2113834_r8
      unit_system%energy%name = "electron volt"
      unit_system%energy%abbrev = "eV"

    case default
      message(1) = "Invalid unit specification."
      call write_fatal(1)
    end select

  end subroutine set_units

end module units_m
