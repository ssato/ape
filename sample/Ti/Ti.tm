# Generalities
Title = "Titanium"
CalculationMode = pp
Verbose = 40

# PseudoPotentials
%PPComponents
  4  |  0  | 2.54 | tm
  4  |  1  | 2.96 | tm
  3  |  2  | 2.25 | tm
%

PPCalculationTolerance = 1e-6
 
LLocal = 0

PPOutputFileFormat = upf

# Wave-equations solver
EigenSolverTolerance = 1e-8
ODEIntTolerance = 1e-14
