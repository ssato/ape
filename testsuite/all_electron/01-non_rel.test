
Test       : All-electron: non-relativistic B
TestGroups : all; ae
Enabled    : Yes


Input: 01-non_rel.01-no_spin.inp

### The energies and eigenvalues are from the NIST database ###
# Etot  = -24.344198
# Ekin  =  24.161047
# Ecoul =  11.503002
# Eenuc = -56.484587
# Exc   =  -3.523660
#
# 1s = -6.564347
# 2s = -0.344701
# 2p = -0.136603

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -24.344198; 2e-06 # Ref. should not be updated
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 24.161047; 2e-06 # Ref. should not be updated
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 11.503002; 10e-07 # Ref. should not be updated
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -56.484587; 3e-06 # Ref. should not be updated
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -3.523660; 1e-12 # Ref. should not be updated

#Eigenvalues
match ; 1s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 2) ; -6.564347; 4e-06 # Ref. should not be updated
match ; 2s Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 3) ; -0.344701; 10e-07 # Ref. should not be updated
match ; 2p Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 4) ; -0.136603; 4e-06 # Ref. should not be updated

#Wavefunctions
match ; 1s Wavefunction            ; LINE(ae/wf-1s, 176, 20) ; 1.94304893E+01; 10e-07
match ; 1s Wavefunction Derivative ; LINE(ae/wf-1s, 176, 39) ; -9.69497404E+01; 5e-06
match ; 2s Wavefunction            ; LINE(ae/wf-2s, 176, 20) ; -4.27631701E+00; 4e-07
match ; 2s Wavefunction Derivative ; LINE(ae/wf-2s, 176, 39) ; 2.15550154E+01; 2e-06
match ; 2p Wavefunction            ; LINE(ae/wf-2p, 176, 20) ; 4.04531116E-02; 5e-09
match ; 2p Wavefunction Derivative ; LINE(ae/wf-2p, 176, 39) ; 3.29014734E+00; 4e-07

#Potentials
match ; v_hxc ; LINE(ae/v_hxc, 176, 20) ; 7.24058113E+00; 6e-07
match ; v_c   ; LINE(ae/v_c,   176, 20) ; -1.18065430E-01; 1e-09
match ; v_x   ; LINE(ae/v_x,   176, 20) ; -3.91832690E+00; 2e-07

#Densities
match ; density       ; LINE(ae/density, 176, 20) ; 6.29985589E+01; 6e-06
match ; grad. density ; LINE(ae/density, 176, 38) ; -6.28945456E+02; 6e-05
match ; lapl. density ; LINE(ae/density, 176, 56) ; -9.90491673E+04; 10e-03
match ; tau           ; LINE(ae/tau,     176, 20) ; 1.57966827E+03; 2e-04


Input: 01-non_rel.02-spin.inp

### The energies and eigenvalues are from the NIST database ###
# Etot  = -24.353614
# Ekin  =  24.172451
# Ecoul =  11.528234
# Eenuc = -56.516740
# Exc   =  -3.537559
#
# 1s = -6.563384
#      -6.550876
# 2s = -0.360441
#      -0.318624
# 2p = -0.150853
#      -0.113184

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -24.353614; 2e-06 # Ref. should not be updated
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 24.172451; 3e-06 # Ref. should not be updated
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 11.528234; 10e-07 # Ref. should not be updated
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -56.516740; 3e-06 # Ref. should not be updated
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -3.537559; 1e-12 # Ref. should not be updated

#Eigenvalues
match ; 1s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 2) ; -6.563384; 4e-06 # Ref. should not be updated
match ; 1s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 3) ; -6.550876; 5e-06 # Ref. should not be updated
match ; 2s_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 4) ; -0.360441; 2e-06 # Ref. should not be updated
match ; 2s_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 5) ; -0.318624; 5e-06 # Ref. should not be updated
match ; 2p_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 6) ; -0.150853; 3e-06 # Ref. should not be updated
match ; 2p_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 32, 7) ; -0.113184; 5e-06 # Ref. should not be updated

#Wavefunctions
match ; 1s_up Wavefunction            ; LINE(ae/wf-1s_up, 176, 20) ; 1.94436041E+01; 5e-07
match ; 1s_up Wavefunction Derivative ; LINE(ae/wf-1s_up, 176, 39) ; -9.70162556E+01; 3e-06
match ; 1s_dn Wavefunction            ; LINE(ae/wf-1s_dn, 176, 20) ; 1.94179634E+01; 2e-06
match ; 1s_dn Wavefunction Derivative ; LINE(ae/wf-1s_dn, 176, 39) ; -9.68862963E+01; 6e-06
match ; 2s_up Wavefunction            ; LINE(ae/wf-2s_up, 176, 20) ; -4.20693727E+00; 2e-06
match ; 2s_up Wavefunction Derivative ; LINE(ae/wf-2s_up, 176, 39) ; 2.12059711E+01; 6e-06
match ; 2s_dn Wavefunction            ; LINE(ae/wf-2s_dn, 176, 20) ; -4.31647219E+00; 4e-07
match ; 2s_dn Wavefunction Derivative ; LINE(ae/wf-2s_dn, 176, 39) ; 2.17566185E+01; 2e-06
match ; 2p_up Wavefunction            ; LINE(ae/wf-2p_up, 176, 20) ; 3.90863313E-02; 2e-08
match ; 2p_up Wavefunction Derivative ; LINE(ae/wf-2p_up, 176, 39) ; 3.17898071E+00; 2e-06
match ; 2p_dn Wavefunction            ; LINE(ae/wf-2p_dn, 176, 20) ; 4.11377531E-02; 8e-09
match ; 2p_dn Wavefunction Derivative ; LINE(ae/wf-2p_dn, 176, 39) ; 3.34583504E+00; 7e-07

#Potentials
match ; v_hxc_up ; LINE(ae/v_hxc_up, 176, 20) ; 7.24728790E+00; 3e-07
match ; v_hxc_dn ; LINE(ae/v_hxc_dn, 176, 20) ; 7.24747908E+00; 3e-07
match ; v_c_up   ; LINE(ae/v_c_up,   176, 20) ; -1.18057466E-01; 3e-09
match ; v_c_dn   ; LINE(ae/v_c_dn,   176, 20) ; -1.18068046E-01; 2e-09
match ; v_x_up   ; LINE(ae/v_x_up,   176, 20) ; -3.91806436E+00; 6e-08
match ; v_x_dn   ; LINE(ae/v_x_dn,   176, 20) ; -3.91786261E+00; 2e-07

#Densities
match ; density_up       ; LINE(ae/density_up, 176, 20) ; 3.14929483E+01; 2e-06
match ; density_dn       ; LINE(ae/density_dn, 176, 20) ; 3.14880834E+01; 4e-06
match ; grad. density_up ; LINE(ae/density_up, 176, 38) ; -3.14419798E+02; 2e-05
match ; grad. density_dn ; LINE(ae/density_dn, 176, 38) ; -3.14348321E+02; 4e-05
match ; lapl. density_up ; LINE(ae/density_up, 176, 56) ; -4.95170901E+04; 2e-03
match ; lapl. density_dn ; LINE(ae/density_dn, 176, 56) ; -4.95039659E+04; 6e-03
match ; tau_up           ; LINE(ae/tau_up,     176, 20) ; 7.88323184E+02; 4e-05
match ; tau_dn           ; LINE(ae/tau_dn,     176, 20) ; 7.90990668E+02; 9e-05
