
Test     : All-electron: fully-relativistic Ar and C
Programs : ape
TestGroups : all; ae
Enabled  : Yes


Input: 03-rel.01-no_spin.inp

### The energies and eigenvalues are from the NIST database.          ###
### Note that there is a quite large differences in the kinetic and   ###
### electron-nucleous interactions, although they seem to cancel      ###
### exactly. This should be further investigated.                     ###
# Etot  =  -527.519049
# Ekin  =   529.722274 	
# Ecoul =   232.084684
# Eenuc = -1260.276935
# Exc   =   -29.049073
#
# 1s = -114.077082
# 2s =  -10.860926
# 2p =   -8.496195
#        -8.414503
# 3s =   -0.890597
# 3p =   -0.386117
#        -0.379536

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -527.519049; 3e-06 # Ref. should not be updated
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 529.722274; 3e-05 # Ref. should not be updated
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 232.084684; 1e-06 # Ref. should not be updated
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -1260.276935; 3e-05 # Ref. should not be updated
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -29.049073; 2e-06 # Ref. should not be updated

#Eigenvalues
match ; 1s0.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 2)  ; -114.077082; 3e-06 # Ref. should not be updated
match ; 2s0.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 3)  ; -10.860926; 5e-06 # Ref. should not be updated
match ; 2p0.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 4)  ; -8.496195; 10e-06 # Ref. should not be updated
match ; 2p1.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 5)  ; -8.414503; 3e-06 # Ref. should not be updated
match ; 3s0.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 6)  ; -0.890597; 3e-06 # Ref. should not be updated
match ; 3p0.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 7)  ; -0.386117; 4e-06 # Ref. should not be updated
match ; 3p1.5 Eigenvalue ; GREP(ae/info, 'Eigenvalues', 27, 8)  ; -0.379536; 5e-06 # Ref. should not be updated

#Wavefunctions
match ; 1s0.5 Wavefunction Large ; LINE(ae/wf-1s0.5, 498, 20) ; 1.63872307E-01; 6e-08
match ; 1s0.5 Wavefunction Small ; LINE(ae/wf-1s0.5, 498, 39) ; -9.89510910E-03; 4e-09
match ; 2s0.5 Wavefunction Large ; LINE(ae/wf-2s0.5, 498, 20) ; 3.92655658E+00; 2e-08
match ; 2s0.5 Wavefunction Small ; LINE(ae/wf-2s0.5, 498, 39) ; -5.26300663E-02; 7e-10
match ; 2p0.5 Wavefunction Large ; LINE(ae/wf-2p0.5, 498, 20) ; 3.53232238E+00; 1e-12
match ; 2p0.5 Wavefunction Small ; LINE(ae/wf-2p0.5, 498, 39) ; 1.06066344E-02; 4e-10
match ; 2p1.5 Wavefunction Large ; LINE(ae/wf-2p1.5, 498, 20) ; 3.53826241E+00; 3e-08
match ; 2p1.5 Wavefunction Small ; LINE(ae/wf-2p1.5, 498, 39) ; -8.62770555E-02; 5e-10
match ; 3s0.5 Wavefunction Large ; LINE(ae/wf-3s0.5, 498, 20) ; -7.92703093E-01; 5e-08
match ; 3s0.5 Wavefunction Small ; LINE(ae/wf-3s0.5, 498, 39) ; 2.79345166E-02; 2e-09
match ; 3p0.5 Wavefunction Large ; LINE(ae/wf-3p0.5, 498, 20) ; -4.57314556E-01; 3e-08
match ; 3p0.5 Wavefunction Small ; LINE(ae/wf-3p0.5, 498, 39) ; 1.36953871E-02; 2e-09
match ; 3p1.5 Wavefunction Large ; LINE(ae/wf-3p1.5, 498, 20) ; -4.67152805E-01; 3e-08
match ; 3p1.5 Wavefunction Small ; LINE(ae/wf-3p1.5, 498, 39) ; 2.62059287E-02; 7e-10

#Potentials
match ; v_hxc ; LINE(ae/v_hxc, 467, 20) ; 3.58050669E+01; 2e-07
match ; v_c   ; LINE(ae/v_c,   467, 20) ; -1.11053597E-01; 1e-12
match ; v_x   ; LINE(ae/v_x,   467, 20) ; -3.05264935E+00; 2e-08

#Densities
match ; density       ; LINE(ae/density, 467, 20) ; 3.02339026E+01; 5e-07
match ; grad. density ; LINE(ae/density, 467, 38) ; -1.75251849E+02; 10e-06
match ; lapl. density ; LINE(ae/density, 467, 56) ; -6.29594374E+02; 9e-04
match ; tau           ; LINE(ae/tau,     467, 20) ; 1.70168584E+03; 4e-05


Input: 03-rel.02-spin.inp

#Energies
match ; Etot  ; GREP(ae/info, 'Energies', 40, 1) ; -37.478439; 4e-05
match ; Ekin  ; GREP(ae/info, 'Energies', 40, 2) ; 37.267909; 4e-05
match ; Ecoul ; GREP(ae/info, 'Energies', 40, 3) ; 17.727092; 6e-06
match ; Eenuc ; GREP(ae/info, 'Energies', 40, 4) ; -87.690830; 9e-06
match ; Exc   ; GREP(ae/info, 'Energies', 40, 5) ; -4.782610; 3e-6

#Eigenvalues
match ; 1s-0.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  2)  ; -9.93880; 3e-05
match ; 1s+0.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  3)  ; -9.90408; 1e-05
match ; 2s-0.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  4)  ; -0.53148; 2e-05
match ; 2s+0.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  5)  ; -0.43528; 2e-05
match ; 2p+0.5_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  6)  ; -0.22758; 2e-05
match ; 2p-0.5_up Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  7)  ; -0.22746; 1e-12
match ; 2p-1.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  8)  ; -0.22735; 1e-12
match ; 2p-0.5_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35,  9)  ; -0.13931; 2e-05
match ; 2p+0.5_dn Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35, 10)  ; -0.13922; 2e-05
match ; 2p+1.5    Eigenvalue ; GREP(ae/info, 'Eigenvalues', 35, 11)  ; -0.13912; 2e-05

#Wavefunctions
match ; 1s-0.5    Wavefunction Large   ; LINE(ae/wf-1s-0.5,    293, 20) ; 1.02141756E-01; 3e-07
match ; 1s-0.5    Wavefunction Small   ; LINE(ae/wf-1s-0.5,    293, 39) ; -1.88778219E-03; 2e-09
match ; 1s+0.5    Wavefunction Large   ; LINE(ae/wf-1s+0.5,    293, 20) ; 1.00171017E-01; 6e-07
match ; 1s+0.5    Wavefunction Small   ; LINE(ae/wf-1s+0.5,    293, 39) ; -1.85943430E-03; 6e-09
match ; 2s-0.5    Wavefunction Large   ; LINE(ae/wf-2s-0.5,    293, 20) ; 7.69165228E-01; 5e-07
match ; 2s-0.5    Wavefunction Small   ; LINE(ae/wf-2s-0.5,    293, 39) ; -1.65499620E-03; 2e-08
match ; 2s+0.5    Wavefunction Large   ; LINE(ae/wf-2s+0.5,    293, 20) ; 7.44609128E-01; 8e-06
match ; 2s+0.5    Wavefunction Small   ; LINE(ae/wf-2s+0.5,    293, 39) ; -1.52311602E-03; 4e-08
match ; 2p+0.5_up Wavefunction Large + ; LINE(ae/wf-2p+0.5_up, 293, 20) ; 3.96075157E-01; 2e-06
match ; 2p+0.5_up Wavefunction Small + ; LINE(ae/wf-2p+0.5_up, 293, 38) ; -2.55261742E-03; 2e-08
match ; 2p+0.5_up Wavefunction Large - ; LINE(ae/wf-2p+0.5_up, 293, 56) ; 5.62263107E-01; 2e-06
match ; 2p+0.5_up Wavefunction Large - ; LINE(ae/wf-2p+0.5_up, 293, 74) ; 2.31214536E-03; 8e-09
match ; 2p-0.5_up Wavefunction Large + ; LINE(ae/wf-2p-0.5_up, 293, 20) ; 5.60766167E-01; 2e-06
match ; 2p-0.5_up Wavefunction Small + ; LINE(ae/wf-2p-0.5_up, 293, 38) ; -3.61386578E-03; 3e-08
match ; 2p-0.5_up Wavefunction Large - ; LINE(ae/wf-2p-0.5_up, 293, 56) ; 3.98013983E-01; 1e-06
match ; 2p-0.5_up Wavefunction Large - ; LINE(ae/wf-2p-0.5_up, 293, 74) ; 1.63683262E-03; 6e-09
match ; 2p-1.5    Wavefunction Large   ; LINE(ae/wf-2p-1.5,    293, 20) ; 6.87557116E-01; 2e-06
match ; 2p-1.5    Wavefunction Small   ; LINE(ae/wf-2p-1.5,    293, 39) ; -4.43079218E-03; 3e-08
match ; 2p-0.5_dn Wavefunction Large + ; LINE(ae/wf-2p-0.5_dn, 293, 20) ; -3.67940406E-01; 6e-06
match ; 2p-0.5_dn Wavefunction Small + ; LINE(ae/wf-2p-0.5_dn, 293, 38) ; 2.30458454E-03; 5e-08
match ; 2p-0.5_dn Wavefunction Large - ; LINE(ae/wf-2p-0.5_dn, 293, 56) ; 5.18653843E-01; 9e-06
match ; 2p-0.5_dn Wavefunction Large - ; LINE(ae/wf-2p-0.5_dn, 293, 74) ; 2.22661321E-03; 3e-08
match ; 2p+0.5_up Wavefunction Large + ; LINE(ae/wf-2p+0.5_dn, 293, 20) ; -5.19680112E-01; 9e-06
match ; 2p+0.5_up Wavefunction Small + ; LINE(ae/wf-2p+0.5_dn, 293, 38) ; 3.25492339E-03; 7e-08
match ; 2p+0.5_up Wavefunction Large - ; LINE(ae/wf-2p+0.5_dn, 293, 56) ; 3.66262924E-01; 6e-06
match ; 2p+0.5_up Wavefunction Large - ; LINE(ae/wf-2p+0.5_dn, 293, 74) ; 1.57243541E-03; 2e-08
match ; 2p+1.5    Wavefunction Large   ; LINE(ae/wf-2p+1.5,    293, 20) ; 6.35646934E-01; 1e-05
match ; 2p+1.5    Wavefunction Small   ; LINE(ae/wf-2p+1.5,    293, 39) ; -3.98116246E-03; 8e-08

#Potentials
match ; v_hxc_up ; LINE(ae/v_hxc_up, 293, 20) ; 4.09121695E+00; 3e-06
match ; v_hxc_dn ; LINE(ae/v_hxc_dn, 293, 20) ; 3.95308805E+00; 4e-07
match ; v_c_up   ; LINE(ae/v_c_up,   293, 20) ; -8.80992293E-02; 6e-07
match ; v_c_dn   ; LINE(ae/v_c_dn,   293, 20) ; -4.99811703E-02; 3e-07
match ; v_x_up   ; LINE(ae/v_x_up,   293, 20) ; -4.40970957E-01; 3e-06
match ; v_x_dn   ; LINE(ae/v_x_dn,   293, 20) ; -6.17217919E-01; 6e-07

#Densities
match ; density_up       ; LINE(ae/density_up, 293, 20) ; 4.49211188E-02; 9e-07
match ; density_dn       ; LINE(ae/density_dn, 293, 20) ; 1.23171802E-01; 4e-07
match ; grad. density_up ; LINE(ae/density_up, 293, 38) ; -5.75996255E-02; 2e-06
match ; grad. density_dn ; LINE(ae/density_dn, 293, 38) ; -1.84636590E-01; 2e-06
match ; lapl. density_up ; LINE(ae/density_up, 293, 56) ; -1.27429181E-01; 3e-04
match ; lapl. density_dn ; LINE(ae/density_dn, 293, 56) ; -2.50360845E-01; 5e-6
match ; tau_up           ; LINE(ae/tau_up,     293, 20) ; 0.00000000E+00; 1e-12
match ; tau_dn           ; LINE(ae/tau_dn,     293, 20) ; 0.00000000E+00; 1e-12
