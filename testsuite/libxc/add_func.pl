#!/usr/bin/env perl
use strict;
use warnings;

die "Usage: $0 executable func_id func_family func_type func_name\n" if($#ARGV!=4);
my $ape = $ARGV[0];
my $func_id = $ARGV[1];
my $func_family = $ARGV[2];
my $func_type = $ARGV[3];
my $func_name = $ARGV[4];

my $func_symbol;
if ($func_name eq " ") {
  $func_symbol = "$func_family\_$func_type";
} else {
  $func_symbol = "$func_family\_$func_type\_$func_name";
};

open(my $input, ">", "$func_id-$func_symbol.inp") or die "Can't open : $func_id-$func_symbol.inp: $!";
if ($func_type eq "k") {

  print $input "# \$Id:  \$

CalculationMode = xc
Verbose = 40

KEDFunctional = $func_symbol
XCFunctional = none
";
} else {
  print $input "# \$Id:  \$

CalculationMode = xc
Verbose = 40

XCFunctional = $func_symbol
";
};
close($input);

`git add $func_id-$func_symbol.inp`;

if ($func_type eq "k") {
  &update_k_test("01-H");
  &update_k_test_spin("02-H_spin");
  &update_k_test("03-He");
  &update_k_test_spin("04-He_spin");
  &update_k_test_spin("05-Li");
} else {
  &update_test("01-H");
  &update_test_spin("02-H_spin");
  &update_test("03-He");
  &update_test_spin("04-He_spin");
  &update_test_spin("05-Li");
};

sub update_test(){
  my $system = $_[0];
  `rm -rf tmp`;
  `mkdir tmp`;
  `cd tmp; $ape < ../$system.inp`;
  `cd tmp; $ape < ../$func_id-$func_symbol.inp`;

  my $energy=`grep energy tmp/xc/info | awk \'{print \$4}\'`;
  my $pot1=`head -n 8 tmp/xc/v_$func_type | tail -n1 | awk \'{print \$2}\'`;
  my $pot2=`head -n 162 tmp/xc/v_$func_type | tail -n1 | awk \'{print \$2}\'`;
  my $pot3=`head -n 207 tmp/xc/v_$func_type | tail -n1 | awk \'{print \$2}\'`;

  chomp($energy); chomp($pot1); chomp($pot2); chomp($pot3);

  open(my $test, ">>", "$system.test") or die "Can't open $system.test: $!";
  print $test "\nInput: $func_id-$func_symbol.inp\n\n";
  print $test "match ; E$func_type ; GREP(xc/info, \"Exchange-correlation energy\", 35) ; $energy       ; 1e-64\n";
  print $test "match ; v$func_type(r1)   ; LINE(xc/v_$func_type, 8,   20)                      ; $pot1 ; 1e-64\n";
  print $test "match ; v$func_type(ri)   ; LINE(xc/v_$func_type, 162, 20)                      ; $pot2 ; 1e-64\n";
  print $test "match ; v$func_type(rmax) ; LINE(xc/v_$func_type, 207, 20)                      ; $pot3 ; 1e-64\n\n";
  close($test);

  `rm -rf tmp`;
};

sub update_test_spin(){
  my $system = $_[0];
  `rm -rf tmp`;
  `mkdir tmp`;
  `cd tmp; $ape < ../$system.inp`;
  `cd tmp; $ape < ../$func_id-$func_symbol.inp`;

  my $energy=`grep energy tmp/xc/info | awk \'{print \$4}\'`;
  my $pot1=`head -n 8 tmp/xc/v_$func_type\_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot2=`head -n 8 tmp/xc/v_$func_type\_dn | tail -n1 | awk \'{print \$2}\'`;
  my $pot3=`head -n 162 tmp/xc/v_$func_type\_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot4=`head -n 162 tmp/xc/v_$func_type\_dn | tail -n1 | awk \'{print \$2}\'`;
  my $pot5=`head -n 207 tmp/xc/v_$func_type\_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot6=`head -n 207 tmp/xc/v_$func_type\_dn | tail -n1 | awk \'{print \$2}\'`;

  chomp($energy); chomp($pot1); chomp($pot2); chomp($pot3); chomp($pot4); chomp($pot5); chomp($pot6);

  open(my $test, ">>", "$system.test") or die "Can't open $system.test: $!";
  print $test "\nInput: $func_id-$func_symbol.inp\n\n";
  print $test "match ; E$func_type ; GREP(xc/info, \"Exchange-correlation energy\", 35) ; $energy       ; 1e-64\n";
  print $test "match ; v$func_type\_up(r1)   ; LINE(xc/v_$func_type\_up, 8,   20)                ; $pot1 ; 1e-64\n";
  print $test "match ; v$func_type\_dn(r1)   ; LINE(xc/v_$func_type\_dn, 8,   20)                ; $pot2 ; 1e-64\n";
  print $test "match ; v$func_type\_up(ri)   ; LINE(xc/v_$func_type\_up, 162, 20)                ; $pot3 ; 1e-64\n";
  print $test "match ; v$func_type\_dn(ri)   ; LINE(xc/v_$func_type\_dn, 162, 20)                ; $pot4 ; 1e-64\n";
  print $test "match ; v$func_type\_up(rmax) ; LINE(xc/v_$func_type\_up, 207, 20)                ; $pot5 ; 1e-64\n";
  print $test "match ; v$func_type\_dn(rmax) ; LINE(xc/v_$func_type\_dn, 207, 20)                ; $pot6 ; 1e-64\n\n";
  close($test);

  `rm -rf tmp`;
}


sub update_k_test(){
  my $system = $_[0];
  `rm -rf tmp`;
  `mkdir tmp`;
  `cd tmp; $ape < ../$system.inp`;
  `cd tmp; $ape < ../$func_id-$func_symbol.inp`;

  my $energy=`grep -A1 \"Kinetic Energy\" tmp/xc/info | tail -n1 | awk \'{print \$2}\'`;
  my $pot1=`head -n 9 tmp/xc/app_tau | tail -n1 | awk \'{print \$2}\'`;
  my $pot2=`head -n 163 tmp/xc/app_tau | tail -n1 | awk \'{print \$2}\'`;
  my $pot3=`head -n 208 tmp/xc/app_tau | tail -n1 | awk \'{print \$2}\'`;

  chomp($energy); chomp($pot1); chomp($pot2); chomp($pot3);

  open(my $test, ">>", "$system.test") or die "Can't open $system.test: $!";
  print $test "\nInput: $func_id-$func_symbol.inp\n\n";
  print $test "match ; Ek ; GREP(xc/info, \"Kinetic Energy\", 35, 1) ; $energy       ; 1e-64\n";
  print $test "match ; tau(r1)   ; LINE(xc/app_tau, 9,   20)       ; $pot1 ; 1e-64\n";
  print $test "match ; tau(ri)   ; LINE(xc/app_tau, 163, 20)       ; $pot2 ; 1e-64\n";
  print $test "match ; tau(rmax) ; LINE(xc/app_tau, 208, 20)       ; $pot3 ; 1e-64\n\n";
  close($test);

  `rm -rf tmp`;
};

sub update_k_test_spin(){
  my $system = $_[0];
  `rm -rf tmp`;
  `mkdir tmp`;
  `cd tmp; $ape < ../$system.inp`;
  `cd tmp; $ape < ../$func_id-$func_symbol.inp`;

  my $energy=`grep -A1 \"Kinetic Energy\" tmp/xc/info | tail -n1 | awk \'{print \$2}\'`;
  my $pot1=`head -n 9 tmp/xc/app_tau_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot2=`head -n 9 tmp/xc/app_tau_dn | tail -n1 | awk \'{print \$2}\'`;
  my $pot3=`head -n 163 tmp/xc/app_tau_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot4=`head -n 163 tmp/xc/app_tau_dn | tail -n1 | awk \'{print \$2}\'`;
  my $pot5=`head -n 208 tmp/xc/app_tau_up | tail -n1 | awk \'{print \$2}\'`;
  my $pot6=`head -n 208 tmp/xc/app_tau_dn | tail -n1 | awk \'{print \$2}\'`;

  chomp($energy); chomp($pot1); chomp($pot2); chomp($pot3); chomp($pot4); chomp($pot5); chomp($pot6);

  open(my $test, ">>", "$system.test") or die "Can't open $system.test: $!";
  print $test "\nInput: $func_id-$func_symbol.inp\n\n";
  print $test "match ; Ek ; GREP(xc/info, \"Kinetic Energy\", 35, 1) ; $energy       ; 1e-64\n";
  print $test "match ; tau_up(r1)   ; LINE(xc/app_tau_up, 9,   20) ; $pot1 ; 1e-64\n";
  print $test "match ; tau_dn(r1)   ; LINE(xc/app_tau_dn, 9,   20) ; $pot2 ; 1e-64\n";
  print $test "match ; tau_up(ri)   ; LINE(xc/app_tau_up, 163, 20) ; $pot3 ; 1e-64\n";
  print $test "match ; tau_dn(ri)   ; LINE(xc/app_tau_dn, 163, 20) ; $pot4 ; 1e-64\n";
  print $test "match ; tau_up(rmax) ; LINE(xc/app_tau_up, 208, 20) ; $pot5 ; 1e-64\n";
  print $test "match ; tau_dn(rmax) ; LINE(xc/app_tau_dn, 208, 20) ; $pot6 ; 1e-64\n\n";
  close($test);

  `rm -rf tmp`;
}
